# How to run the app

## clone the repo:
```
https://gitlab.com/Halladj/4-colarable
```

## get into the repo:
```
cd 4-colarable
```

## install dependencies:
```
pip install numpy

pip install pandas

pip install networkx
```

## run the app:
```
python main.py
```
