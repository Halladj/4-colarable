from typing import List, Tuple, Any, Dict

import numpy as np
import pandas as pd 

class Solver:
    def __init__(self):
        self.degreeVector: Dict[int,int]= {}
        self.sortedVector:List[int]= []
        self.edgesLisr:List[Tuple[int,int]]=[]
        self.solution:List[str]=[]
        self.colors:List[str]= [
                "red", "blue", "green", "green", "yellew", "pink",
                "black", "brown", "cyan", "white", "gray", "orange"
                ]


    def createAdjacencyMatrix(self, nodesNumber:int, l:List[Any]):
        self.adjacencyMatrix= np.zeros([nodesNumber,nodesNumber])
        for element in l:
            self.adjacencyMatrix[element[0]-1][element[1]-1]= 1
            self.adjacencyMatrix[element[1]-1][element[0]-1]= 1


    ## creates apriority queue using DEGREE HEURISTIQUE
    def setDegreeVector(self):
        for i in range(len(self.adjacencyMatrix)):
            self.degreeVector[i]=(int(sum(self.adjacencyMatrix[i])))

        hamza= sorted(self.degreeVector.items(), key=lambda x:x[1] )
        for e in hamza :
            self.sortedVector.append(e[0])


    ## Makes Sure No Neighbor node has the same color
    def isCompatible(self, node:int) -> Tuple[bool,str]:
        removedColors=[]
        colors =["red", "blue", "green", "yellow"]
        neighbor= []
        for i in range(len(self.adjacencyMatrix)):
            if self.adjacencyMatrix[node][i] == 1:
                neighbor.append(i)

        print("FOR NODE: "+str(node))
        print("NG>>>",neighbor)

        for i in range (len(colors)):
            for ng in neighbor:
                if self.solution[ng] == colors[i] and (colors[i] not in removedColors):
                    try :
                        print("removing >>>" ,colors[i])
                        removedColors.append(colors[i])
                        print("removing >>>" , removedColors)
                        continue
                    except:
                        return False,""

        if len(colors) == 0:
            return False,""

        temp = [item for item in colors if item not in removedColors]
        print("Options>>>", temp)
        print("selected>>>"+str(temp[0])+"\n")
        return True,temp[0]

    def colorNode(self, node:int, color:str):
        self.solution[node]= color
        

    def solver(self) -> bool:
        frontier = self.sortedVector
        self.solution= [""] * len(frontier)

        while len(frontier) != 0:
            node = frontier.pop()
            err, color= self.isCompatible(node)
            if err != "" :
                self.colorNode(node, color)
            else:
                return False
        return True

    def makeGraph(self, fileName:str)->Tuple[bool, List[str]]:
        nodeNb:int= 0
        l: List[Any]= []
        try:
            dataFrame= pd.read_csv(fileName, sep=' ')
        except:
            print("File not found")
            print("Will open graph.txt Instead")
            dataFrame= pd.read_csv("graph.txt", sep=' ')

        l= list(dataFrame.itertuples(index=False, name=None))
        # Containes nodes numbers & arcs number [NODES, ARCS]
        first=l.pop(0)
        nodeNb= first[0]
        self.edgesLisr=l

        # Creates  adjacency matrix wiht nodeNb*nodeNb dimintions
        self.createAdjacencyMatrix(nodeNb, l)
        # Created sorted by degree list of all nodes
        self.setDegreeVector()

        err= self.solver()

        if err == False:
            return False,self.solution
        else: 
            return True,self.solution


        


