import networkx as nx 
import matplotlib.pyplot as plt
from typing import List, Tuple, Any, Dict



class GraphVisualization:
   
    def __init__(self):
          
        self.colors = []
        self.visual = []

    def set_colors(self, node_color: list[str]):
        self.colors= node_color
          
    def addEdge(self, a:int, b:int):
        temp = [a, b]
        self.visual.append(temp)
          
    def visualize(self):
        G = nx.Graph()
        G.add_edges_from(self.visual)
        nx.draw_networkx(G,node_color=self.colors)
        plt.show()

    def makeGraph(self, values:List[Tuple[int,int]]):
        for v in values:
            self.addEdge(v[0], v[1])

